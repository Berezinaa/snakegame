// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MoveSpeed = 1.f;
	LastMoveDirection = EMoveDirection::UP;
	DefaultSnakeSize = 3;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveSpeed);
	AddSnakeElement(DefaultSnakeSize);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(-1*SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0) {
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	if (SnakeElements.Num() < DefaultSnakeSize)
		return;

	FVector MoveVector(ForceInitToZero);
	float MoveSpeedDelta = ElementSize;
	switch (LastMoveDirection)
	{
	case EMoveDirection::UP:
		MoveVector.X += MoveSpeedDelta;
		break;
	case EMoveDirection::DOWN:
		MoveVector.X -= MoveSpeedDelta;
		break;
	case EMoveDirection::LEFT:
		MoveVector.Y += MoveSpeedDelta;
		break;
	case EMoveDirection::RIGHT:
		MoveVector.Y -= MoveSpeedDelta;
		break;
	default:
		break;
	}


	//AddActorWorldOffset(MoveVector);

	for ( int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MoveVector);
}

